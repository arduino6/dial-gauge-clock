#include <CmdBuffer.hpp>
#include <CmdCallback.hpp>
#include <CmdParser.hpp>
#include <GyverTimer.h>
#include <ThreeWire.h>
#include <RtcDS1302.h>
#include <EEPROM.h>

#define DEBUG(str)
#define DEBUG_ENABLED
#ifdef DEBUG_ENABLED
#undef DEBUG
#define DEBUG(str) Serial.println((str))
#endif
#define SECONDS_PIN (11)
#define MINUTES_PIN (10)
#define HOURS_PIN (9)
#define NORMAL_MODE (0)
#define SETUP_MODE (1)
//#define INIT_EEPROM
#define EMPTY_EEPROM_CRC (0xFFA07F7F)
#define EEPROM_P_SECONDS (0)
#define EEPROM_P_MINUTES (sizeof(seconds))
#define EEPROM_P_HOURS (sizeof(seconds)+sizeof(minutes))

//#define INIT_EEPROM
//Настраиваем часы рельного времени
ThreeWire myWire(A1, A0, A2); // IO, SCLK, CE
RtcDS1302<ThreeWire> Rtc(myWire);
GTimer RtcRefreshTimer(MS, 500);
RtcDateTime currentTime;
uint8_t mode = NORMAL_MODE;
byte seconds[] = {100, 100, 100, 100, 100, 100};
byte minutes[] = {100, 100, 100, 100, 100, 100};
byte hours[] = {100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
//Настраиваем RTC
void initRtc()
{
  Rtc.Begin();
  RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
  if (!Rtc.IsDateTimeValid())
  {
    DEBUG("RTC потеря информации о текущем времени!");
    Rtc.SetDateTime(compiled);
  }
  if (Rtc.GetIsWriteProtected())
  {
    DEBUG("RTC снимаем защиту от записи");
    Rtc.SetIsWriteProtected(false);
  }
  if (!Rtc.GetIsRunning())
  {
    DEBUG("RTC часы не запущены. Запуск часов");
    Rtc.SetIsRunning(true);
  }
  RtcDateTime now = Rtc.GetDateTime();
  if (now < compiled)
  {
    DEBUG("RTC Текущее время часов позже дата компиляции! Обновляем дату");
    Rtc.SetDateTime(compiled);
  }
  else if (now > compiled)
  {
    DEBUG("RTC Текщее время после даты компиляции. (Так и должно быть)");
  }

  else if (now == compiled)
  {
    DEBUG("RTC Текушее время равно дате компиляции ! (Неожиданно, но приемлемо)");
  }

}

// Настраваем пины стрелочных индикаторов
void initIndicators()
{
  pinMode(SECONDS_PIN, OUTPUT);
  pinMode(MINUTES_PIN, OUTPUT);
  pinMode(HOURS_PIN, OUTPUT);
}

CmdCallback_P<4> cmdCallback; //Обработчик команд.
CmdBuffer<32> myBuffer;       // Буфер обработчика
CmdParser     myParser;       // парсер команд
void refreshCurrentTime()
{
  currentTime = Rtc.GetDateTime();
  DEBUG((String)currentTime.Hour() + ":" + currentTime.Minute() + ":" + currentTime.Second());
  displayValue(SECONDS_PIN, currentTime.Second());
}

void fSetup(CmdParser *myParser)
{
  mode = SETUP_MODE;
  Serial.println("Setup mode on");
}
void setupGauge(uint8_t pin, uint8_t index, uint8_t value, byte* pArray, int size)
{
  if (index >= size)
  {
    Serial.println("IoR");
    return;
  }
  pArray[index] = value;
  showGauge(pin, index, pArray, size);
}

void showGauge(uint8_t pin, uint8_t index, byte* pArray, int size)
{
  uint8_t value = pArray[index];
  uint8_t pos = ((uint32_t)value * (index + 1) * 256) / (size * 100);
  Serial.println("i:" + (String) index + " v:" + (String) value + " c:" + (String) pos);
  analogWrite(pin, pos);

}

void fSecondsSetup(CmdParser *myParser)
{
  if (mode != SETUP_MODE)
  {
    Serial.println("need setup mode");
    return;
  }

  switch (myParser->getParamCount())
  {
    case 1:
      DEBUG("wrire min to EEPROM addr: " + (String)EEPROM_P_SECONDS);
      EEPROM.put(EEPROM_P_SECONDS, seconds);
      return;
    case 2:
      {
        uint8_t index = atoi(myParser->getCmdParam(1));
        showGauge(SECONDS_PIN, index, seconds, sizeof(seconds));
        return;
      }
    case 3:
      {
        uint8_t index = atoi(myParser->getCmdParam(1));
        uint8_t value = atoi(myParser->getCmdParam(2));
        setupGauge(SECONDS_PIN, index, value, seconds, sizeof(seconds));
        return;
      }
  }
}

void fHoursSetup(CmdParser *myParser)
{
  if (mode != SETUP_MODE)
  {
    Serial.println("need setup mode");
    return;
  }

  switch (myParser->getParamCount())
  {
    case 1:
      DEBUG("wrire hour to EEPROM addr: " + (String)EEPROM_P_HOURS);
      EEPROM.put(EEPROM_P_HOURS, hours);
      return;
    case 2:
      {
        uint8_t index = atoi(myParser->getCmdParam(1));
        showGauge(HOURS_PIN, index, hours, sizeof(hours));
        return;
      }
    case 3:
      {
        uint8_t index = atoi(myParser->getCmdParam(1));
        uint8_t value = atoi(myParser->getCmdParam(2));
        setupGauge(HOURS_PIN, index, value, hours, sizeof(hours));
        return;
      }
  }
}

void fMinutesSetup(CmdParser *myParser)
{
  if (mode != SETUP_MODE)
  {
    Serial.println("need setup mode");
    return;
  }

  switch (myParser->getParamCount())
  {
    case 1:
      DEBUG("wrire min to EEPROM addr: " + (String)EEPROM_P_MINUTES);
      EEPROM.put(EEPROM_P_MINUTES, minutes);
      return;
    case 2:
      {
        uint8_t index = atoi(myParser->getCmdParam(1));
        showGauge(MINUTES_PIN, index, minutes, sizeof(minutes));
        return;
      }
    case 3:
      {
        uint8_t index = atoi(myParser->getCmdParam(1));
        uint8_t value = atoi(myParser->getCmdParam(2));
        setupGauge(MINUTES_PIN, index, value, minutes, sizeof(minutes));
        return;
      }
  }
}

void fExit(CmdParser *myParser)
{
  mode = NORMAL_MODE;
  Serial.println("Setup mode off");
}
void displayValue(uint8_t indicator, uint8_t value)
{
  analogWrite(indicator, value * 255 / 60);
}

unsigned long eeprom_crc(void) {

  const unsigned long crc_table[16] = {
    0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
    0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
    0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
    0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
  };

  unsigned long crc = ~0L;

  for (int index = 0 ; index < EEPROM.length()  ; ++index) {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }
  return crc;
}
void initGauge()
{
  if (eeprom_crc() != EMPTY_EEPROM_CRC)
  {
    DEBUG ("Read seconds from EEPROM");
    EEPROM.get(EEPROM_P_SECONDS, seconds);
    EEPROM.get(EEPROM_P_MINUTES, minutes);
    EEPROM.get(EEPROM_P_HOURS, hours);
  }
}
void setup() {
  // Инициализируем последовательный порт
  Serial.begin(9600);
  Serial.println("start!");
  Serial.println("compiled: " + (String)__DATE__ + " " + (String)__TIME__);

#ifdef INIT_EEPROM
  for (int i = 0; i < EEPROM.length(); i++) { // Обнуляем EEPROM - приводим в первоначальное состояние
    EEPROM.update(i, 255);
  }
  Serial.println(eeprom_crc(), HEX);          // В HEX-формате
#endif
  initGauge();
  initRtc();
  initIndicators();
  refreshCurrentTime();

  // Ицициализация обработчика команд
  cmdCallback.addCmd(PSTR("setup"), &fSetup);
  cmdCallback.addCmd(PSTR("sec"), &fSecondsSetup);
  cmdCallback.addCmd(PSTR("min"), &fMinutesSetup);
  cmdCallback.addCmd(PSTR("hour"), &fHoursSetup);
  cmdCallback.addCmd(PSTR("exit"), &fExit);
  myBuffer.setEcho(true);
}


void loop() {
  cmdCallback.updateCmdProcessing(&myParser, &myBuffer, &Serial);

  if (mode != NORMAL_MODE)
  {
    return;
  }

  if (RtcRefreshTimer.isReady())
  {
    refreshCurrentTime();
  }
}
